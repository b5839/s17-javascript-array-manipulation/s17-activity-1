/*
Instruction S17 Activity:
3. Create an addStudent() function that will accept a name of the student and add it to the student array.
4. Create a countStudents() function that will print the total number of students in the array.
5. Create a printStudents() function that will sort and individually print the students (sort and forEach methods) in the array.
6. Create a findStudent() function that will do the following:
Search for a student name when a keyword is given (filter method).
If one match is found print the message studentName is an enrollee.
If multiple matches are found print the message studentNames are enrollees.
If no match is found print the message studentName is not an enrollee.
The keyword given should not be case sensitive.
*/

let student = [];

function addStudent(name){
	student.push(name);
	console.log(name + " was added to the student's list.");
}

function countStudents(){
	console.log("There are total of " + student.length + " student enrolled");
}

function printStudents(){
	student.sort();
	student.forEach(function(printName){	
	console.log(printName);
	})
	
}


function findStudent(findName){
	let newArray = student.filter(function(searchStudent){
		return (searchStudent.toLowerCase().includes(findName.toLowerCase()));
	})
	if (newArray.length == 1){
		console.log(newArray + " is an Enrollee");		
	}else if(newArray.length>1){
		console.log(newArray + " are Enrollees");
	} else {
		console.log("No Student found with the name of "+ findName);
		
	}
}
